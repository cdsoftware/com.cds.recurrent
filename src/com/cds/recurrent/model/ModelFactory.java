package com.cds.recurrent.model;

import java.sql.ResultSet;

import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

public class ModelFactory implements IModelFactory{

	@Override
	public Class<?> getClass(String tableName) {
		
		if(tableName.equalsIgnoreCase(MBPartner_Product_Chip.Table_Name))
			return MBPartner_Product_Chip.class;
		
		return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
		if(tableName.equalsIgnoreCase(MBPartner_Product_Chip.Table_Name))
			return new MBPartner_Product_Chip(Env.getCtx(), Record_ID, trxName);
	
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		if(tableName.equalsIgnoreCase(MBPartner_Product_Chip.Table_Name))
			return new MBPartner_Product_Chip(Env.getCtx(), rs, trxName);

		return null;
	}
}