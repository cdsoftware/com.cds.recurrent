package com.cds.recurrent.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

import org.compiere.model.MBPartner;
import org.compiere.model.MDiscountSchema;
import org.compiere.model.MDiscountSchemaBreak;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class MBPartner_Product_Chip extends X_C_BPartner_Product_Chip{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MBPartner_Product_Chip(Properties ctx, int C_BPartner_Product_Chip_ID, String trxName) {
		super(ctx, C_BPartner_Product_Chip_ID, trxName);
		// TODO Auto-generated constructor stub
	}
	
	public MBPartner_Product_Chip(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}

	protected boolean afterSave (boolean newRecord, boolean success) {
		MBPartner bpartner = new MBPartner(getCtx(), getC_BPartner_ID(), get_TrxName());
		if(is_ValueChanged("BreakDiscount") && ((BigDecimal)get_Value("BreakDiscount")).compareTo(BigDecimal.ZERO)!=0) {
				//Verifico si existe Esquema de descuento para el tercero y el producto
				if(getC_BPartner().getM_DiscountSchema_ID()>0) {
					MDiscountSchema DiscSchema = new MDiscountSchema(getCtx(), bpartner.getM_DiscountSchema_ID(), get_TrxName());
					//Busco producto en esquemas actuales
					boolean exists = false;
					MDiscountSchemaBreak[] DiscSchemaLines = DiscSchema.getBreaks(false);
					MDiscountSchemaBreak DiscSchemaLineEx = null;
					for(MDiscountSchemaBreak DiscSchemaLine:DiscSchemaLines) {
						if(DiscSchemaLine.getM_Product_ID()==getM_Product_ID()) {
							DiscSchemaLineEx = DiscSchemaLine;
							exists=true;
							break;
						}
					}	
					if(!exists) {
						//Creo la linea del esquema
						MDiscountSchemaBreak DiscSchemaLine = new MDiscountSchemaBreak(getCtx(), 0, get_TrxName());
						DiscSchemaLine.setM_DiscountSchema_ID(DiscSchema.getM_DiscountSchema_ID());
						if (DiscSchemaLine.getSeqNo() == 0)
						{
							final String sql = "SELECT COALESCE(MAX(SeqNo),0) + 10 FROM "+DiscSchemaLine.Table_Name
												+" WHERE M_DiscountSchema_ID=?";
							int seqNo = DB.getSQLValueEx(get_TrxName(), sql, DiscSchemaLine.getM_DiscountSchema_ID());
							DiscSchemaLine.setSeqNo(seqNo);
						}
						DiscSchemaLine.setAD_Org_ID(getAD_Org_ID());
						DiscSchemaLine.setM_Product_Category_ID(-1);
						DiscSchemaLine.setM_Product_ID(getM_Product_ID());
						DiscSchemaLine.setBreakDiscount((BigDecimal)get_Value("BreakDiscount"));
						DiscSchemaLine.saveEx();						
					}else {
						if(DiscSchemaLineEx!=null) {
							DiscSchemaLineEx.setBreakDiscount((BigDecimal)get_Value("BreakDiscount"));
							DiscSchemaLineEx.saveEx();
						}
					}
				}else {
					MDiscountSchema DiscSchema = new MDiscountSchema(getCtx(), 0, get_TrxName());
					DiscSchema.setName(bpartner.getName());
					DiscSchema.setDiscountType("B");
					DiscSchema.setIsQuantityBased(true);
					DiscSchema.setCumulativeLevel("L");
					DiscSchema.saveEx();
					
					//Creo la linea del esquema
					MDiscountSchemaBreak DiscSchemaLine = new MDiscountSchemaBreak(getCtx(), 0, get_TrxName());
					DiscSchemaLine.setM_DiscountSchema_ID(DiscSchema.getM_DiscountSchema_ID());
					DiscSchemaLine.setAD_Org_ID(getAD_Org_ID());
					DiscSchemaLine.setM_Product_Category_ID(-1);
					DiscSchemaLine.setSeqNo(10);
					DiscSchemaLine.setM_Product_ID(getM_Product_ID());
					DiscSchemaLine.setBreakDiscount((BigDecimal)get_Value("BreakDiscount"));
					DiscSchemaLine.saveEx();
					
					bpartner.setM_DiscountSchema_ID(DiscSchema.getM_DiscountSchema_ID());
					bpartner.saveEx();
				}				
		}
		return success;
		
	}
}
