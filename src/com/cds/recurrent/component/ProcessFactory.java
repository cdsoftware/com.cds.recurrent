package com.cds.recurrent.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import com.cds.recurrent.process.GenerateRecurrent;
import com.cds.recurrent.process.InvoiceGenerate;

public class ProcessFactory implements IProcessFactory{

	@Override
	public ProcessCall newProcessInstance(String className) {
		if(className.equals("com.cds.recurrent.process.GenerateRecurrent"))
			return new GenerateRecurrent();
		if (className.equals("com.cds.recurrent.process.InvoiceGenerate"))
			return new InvoiceGenerate();
		return null;
	}

}
