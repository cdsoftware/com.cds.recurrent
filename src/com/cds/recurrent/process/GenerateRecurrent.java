package com.cds.recurrent.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrg;
import org.compiere.model.MPaymentTerm;
import org.compiere.model.MProduct;
import org.compiere.model.MProductPricing;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import com.cds.recurrent.model.MBPartner_Product_Chip;

import java.time.LocalDate;

public class GenerateRecurrent extends SvrProcess{

	/**	Date Invoiced			*/	
	private Timestamp	p_DateInvoiced = null;
	/**							*/
	private Timestamp 	p_DateInvoice_start = null;
	/**							*/
	private Timestamp 	p_DateInvoice_end = null;
	/** BPartner				*/
	private int			p_C_BPartner_ID = 0;
	/** Invoice Document Action	*/
	private String		p_docAction = DocAction.ACTION_Prepare;
	/**							*/
	private Timestamp 	p_DateStartServ = null;
	/**	The current Order	*/
	private MOrder 	order = null;
	/**	The current Shipment	*/
	private int			m_created = 0;
	/**	Document Type		*/
	private int			p_C_DocType_ID = 0;
	private int FrequencyType=Calendar.MONTH;
	private int FrequencyRate=1;

	private ArrayList<InvoiceDataRecurrent> invoicesdata = new ArrayList<InvoiceDataRecurrent>();

	private Calendar fcierre;
	private int ProductDiscountID = 0;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();

			if (para[i].getParameter() == null)
				;
			else if (name.equals("DateInvoiced"))
				p_DateInvoiced = (Timestamp)para[i].getParameter();
			else if (name.equals("C_BPartner_ID"))
				p_C_BPartner_ID = para[i].getParameterAsInt();
			else if (name.equals("C_DocType_ID"))
				p_C_DocType_ID = para[i].getParameterAsInt();
			else if (name.equals("DocAction"))
				p_docAction = para[i].getParameterAsString();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		//	Login Date
		if (p_DateInvoiced == null)
			p_DateInvoiced = Env.getContextAsDate(getCtx(), "#Date");
		if (p_DateInvoiced == null)
			p_DateInvoiced = new Timestamp(System.currentTimeMillis());

		ProductDiscountID = MSysConfig.getIntValue("RECURRENT_PRODUCT_DISCOUNT_ID", 0, getAD_Client_ID());
	}

	@Override
	protected String doIt() throws Exception {

		/** CALCULAR LA FECHA CANTIDAD DE DIAS Y FIN DE MES*/
		fcierre = Calendar.getInstance();
		fcierre.setTimeInMillis(p_DateInvoiced.getTime());	
		fcierre.set( Calendar.DAY_OF_MONTH, fcierre.getActualMaximum(Calendar.DAY_OF_MONTH));
		Calendar DateInvoiced = Calendar.getInstance();
		DateInvoiced.setTimeInMillis(p_DateInvoiced.getTime());
		DateInvoiced.set(Calendar.DAY_OF_MONTH,1);		
		p_DateInvoice_start = new Timestamp(DateInvoiced.getTimeInMillis());
		DateInvoiced.set(Calendar.DAY_OF_MONTH,fcierre.getActualMaximum(Calendar.DAY_OF_MONTH));
		p_DateInvoice_end = new Timestamp(DateInvoiced.getTimeInMillis());		
		if (log.isLoggable(Level.INFO)) log.info("C_BPartner_ID=" + p_C_BPartner_ID + ", DateInvoiced=" + p_DateInvoiced);
		StringBuilder sql = null;
		sql = new StringBuilder("SELECT f.AD_Client_ID, f.AD_Org_ID, f.contract_no, f.Bill_BPartner_ID,f.Bill_Location_ID,f.Bill_User_ID, f.service_start_date, M_PriceList.C_Currency_ID,")
				.append("f.service_finish_date, f.FrequencyType, f.m_product_id, f.c_bpartner_location_id, p.PaymentRule, p.C_PaymentTerm_ID, ")
				.append("f.ad_User_id, p.c_bpartner_id, p.name, p.SalesRep_ID, p.M_PriceList_ID,  c_bpartner_Product_chip_ID, f.a_asset_id,")
				.append("f.m_pricelist_id as pricef, l.name as namel, product.name as namep, l.c_bpartner_location_id as locationbill, ")
				.append("cl.address1, f.qty,at.description as atributeDescription,f.description as chipdescription,lastrun,f.breakValue ")
				.append("FROM C_BPartner_Product_Chip f ")
				.append("INNER JOIN C_BPartner p on p.c_bpartner_id = f.c_bpartner_id ")
				.append("INNER JOIN M_PriceList on M_PriceList.M_PriceList_id = f.M_PriceList_ID ")
				.append("INNER JOIN m_product product on product.m_product_id = f.m_product_id ")
				.append("INNER JOIN C_BPartner_Location l on l.c_bpartner_location_id = f.Bill_Location_ID ")
				.append("INNER JOIN C_Location cl on l.c_location_id = cl.c_location_id ")
				.append("LEFT OUTER JOIN M_AttributeSetInstance at ON at.M_AttributeSetInstance_ID=f.M_AttributeSetInstance_ID ")
				.append("WHERE f.M_PriceList_ID!=0 and f.FrequencyType is not null and f.isactive='Y' and p.isactive='Y' " +
						"and f.service_start_date <= ? and (service_finish_date is null or service_finish_date >= ?) ");
		if (p_C_BPartner_ID != 0)
			sql.append("AND p.C_BPartner_ID = ? ");
		sql.append("AND f.Bill_BPartner_ID NOT IN (SELECT i.c_bpartner_id FROM c_order i ")
		.append("INNER JOIN c_orderline l ON f.m_product_id = l.m_product_id AND l.c_order_id = i.c_order_id  ")
		.append("WHERE i.c_bpartner_id = f.c_bpartner_id AND (DocStatus = 'CO' OR DocStatus = 'DR' OR DocStatus = 'IP') ")
		.append("AND i.C_DoctypeTarget_ID = ? AND i.dateordered BETWEEN ? AND ? )  ")
		.append("AND f.AD_Client_ID = ? ")
		.append("ORDER BY p.c_bpartner_id, f.Bill_BPartner_ID, pricef, f.Bill_User_ID, f.m_product_id");

		PreparedStatement pstmt = null;
		try
		{
			pstmt = DB.prepareStatement (sql.toString(), get_TrxName());
			int index = 1;
			if (p_DateInvoiced != null){ // para la comprobar la fecha de inicio de serv en ficha del producto
				pstmt.setTimestamp(index++, p_DateInvoiced);
			}
			if(p_DateInvoice_end != null){ // para la comprobar la fecha de fin de serv en ficha del producto
				pstmt.setTimestamp(index++, p_DateInvoice_end);
			}
			if (p_C_BPartner_ID != 0)
				pstmt.setInt(index++, p_C_BPartner_ID);

			if(p_C_DocType_ID != 0)
				pstmt.setInt(index++, p_C_DocType_ID);

			if (p_DateInvoice_start != null){ // para la comprobar la fecha en invoice
				pstmt.setTimestamp(index++, p_DateInvoice_start);
			}
			if(p_DateInvoice_end != null){  // para la comprobar la fecha en invoice

				pstmt.setTimestamp(index++, p_DateInvoice_end);
			}
			pstmt.setInt(index++, this.getAD_Client_ID());

		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql.toString(), e);
		}

		return generate(pstmt);
	}

	/********************************************************************/
	private String generate (PreparedStatement pstmt)
	{
		ResultSet rs = null;		
		String locationname= "", productname ="", paymentrule = "", description = "",licenseplate="", atributeDescription="",contract_no = "",chipdescription = "" ;
		int idorg = 0, iduser = 0,  idproduct = 0, idcbpartner = 0, idbillpartner = 0, idpricelist = 0, 
				idpartnerlocation = 0, idproductchip = 0,SalesRep_ID=0,C_PaymentTerm_ID=0, idbilluser = 0, idbilllocation = 0;
		//temp variables
		int temp_user = -1, temp_cp= -1, temp_price = -1 , temp_bilcp= -1;
		String temp_contract_no = "";
		int a_asset_id =0 ;
		int CurrencyID = 0;
		StringBuilder msgreturn;
		int qty = 0;
		//Timestamp lastrun = null;
		BigDecimal breakValue=Env.ZERO;
		try
		{
			rs = pstmt.executeQuery();
			while (rs.next ())
			{
				this.statusUpdate("Generando Órden "+rs.getRow());
				idorg = rs.getInt("AD_Org_ID");
				iduser= rs.getInt("ad_User_id");
				idbillpartner =  rs.getInt("Bill_BPartner_ID") ;
				if(idbillpartner != 0){
					idcbpartner = rs.getInt("Bill_BPartner_ID");
				}
				idpricelist = rs.getInt("pricef");
				if( rs.getString("PaymentRule") != null)
					paymentrule = rs.getString("PaymentRule");
				if( rs.getInt("C_PaymentTerm_ID")!=0)
					C_PaymentTerm_ID = rs.getInt("C_PaymentTerm_ID");
				else
					C_PaymentTerm_ID = new Query(getCtx(), MPaymentTerm.Table_Name, "IsDefault = 'Y'", get_TrxName()).setClient_ID().firstId();			
				description = "";
				atributeDescription="";
				chipdescription = "";
				idproduct = rs.getInt("M_Product_ID");
				productname = rs.getString("namep");
				idproductchip= rs.getInt("c_bpartner_Product_chip_ID");
				CurrencyID = rs.getInt("C_Currency_ID");
				qty = rs.getInt("qty");

				if(rs.getString("a_asset_id") != null)
					a_asset_id = rs.getInt("a_asset_id");

				if(rs.getString("SalesRep_ID")!=null)	
					SalesRep_ID=rs.getInt("SalesRep_ID");
				else
					SalesRep_ID = this.getAD_User_ID();

				if(rs.getString("Bill_Location_ID")!=null){	
					idbilllocation=rs.getInt("Bill_Location_ID");
					idpartnerlocation=rs.getInt("Bill_Location_ID");
					locationname = rs.getString("address1");
				}

				if(rs.getString("Bill_User_ID")!=null)	
					idbilluser=rs.getInt("Bill_User_ID");

				if(rs.getString("atributeDescription")!=null)
					atributeDescription = rs.getString("atributeDescription");

				if(rs.getString("contract_no")!=null)
					contract_no = rs.getString("contract_no");

				if(rs.getString("chipdescription")!=null)
					chipdescription = rs.getString("chipdescription");

				/*if(rs.getTimestamp("lastrun")!=null)
					lastrun = rs.getTimestamp("lastrun");
				else
					lastrun=rs.getTimestamp("service_start_date");*/
				if(rs.getBigDecimal("breakValue")!=null)
					breakValue = rs.getBigDecimal("breakValue");
				if(checkFrequencyType(rs.getInt("c_bpartner_Product_chip_ID")))
				{
					invoicesdata.add(new InvoiceDataRecurrent(idorg, iduser, CurrencyID, idcbpartner, idbillpartner, idproduct, productname,
							paymentrule,idpricelist, idpartnerlocation, p_C_DocType_ID, locationname, description, idproductchip, licenseplate,
							SalesRep_ID,C_PaymentTerm_ID,a_asset_id,idbilluser,idbilllocation,qty,atributeDescription,contract_no,chipdescription,
							breakValue));
				}
				commitEx();
			}

			/**INVOICES **/
			if(invoicesdata.size() >= 0){
				int y = 0, i = 0, size =invoicesdata.size();

				for(i=0; i< size; i++){
					idcbpartner = invoicesdata.get(i).getIDCBPartner();
					idbillpartner = invoicesdata.get(i).getIDBillPartner();
					idpricelist = invoicesdata.get(i).getIDPriceList();
					iduser = invoicesdata.get(i).getBill_User_ID();
					contract_no = invoicesdata.get(i).getcontract_no();
					/**Condiciciones para nueva factura
					cambia el tercero
					cambia el tercero a facturar
					cambia la lista de precios
					cambia el usuario
					cambia el nro de contrato**/
					if(idcbpartner != temp_cp 
							|| idbillpartner != temp_bilcp 
							|| idpricelist != temp_price 
							|| iduser != temp_user
							|| contract_no.compareTo(temp_contract_no)!=0){
						if(i > 0){
							CreateInvoice(y, i);
							y=i;
						}			
						order = new MOrder(getCtx(), 0, get_TrxName());
						order.setAD_Org_ID(invoicesdata.get(i).getIDOrg());
						order.setAD_User_ID(invoicesdata.get(i).getIDUser());
						if(idbillpartner == 0)
							order.setC_BPartner_ID(idcbpartner);
						else
							order.setC_BPartner_ID(idbillpartner);
						order.setC_BPartner_Location_ID(invoicesdata.get(i).getIDPartnerLocation());
						order.setM_PriceList_ID(invoicesdata.get(i).getIDPriceList());
						order.setDateOrdered(p_DateInvoiced);
						order.setC_DocTypeTarget_ID(invoicesdata.get(i).getIDDoctype());
						order.setC_Currency_ID(invoicesdata.get(i).getIDCurrency());
						order.setPaymentRule(invoicesdata.get(i).getPaymentRule());
						order.addDescription(invoicesdata.get(i).getDescription());
						order.setSalesRep_ID(invoicesdata.get(i).getSalesRep_ID());
						order.setC_PaymentTerm_ID(invoicesdata.get(i).getC_PaymentTerm_ID());
						order.setPaymentRule("P");
						order.setM_Warehouse_ID(MOrg.get(getCtx(), order.getAD_Org_ID()).getInfo().getM_Warehouse_ID());
						if (!order.save()){
							log.warning("No se pudo crear orden para ficha:"+invoicesdata.get(i).idproductchip);
							DB.close(rs, pstmt);
							rs = null;
							pstmt = null;
							throw new Exception("No se pudo crear orden para ficha:"+invoicesdata.get(i).idproductchip);
						}

					}
					temp_cp = idcbpartner;
					temp_price = idpricelist;
					temp_user = iduser;
					temp_bilcp = idbillpartner;
					temp_contract_no = contract_no;
				}
				if(i > 0)
					CreateInvoice(y, i);

				updateChipLastRun();	
			}/**fin if**/
			msgreturn = new StringBuilder("@Created@ = ").append(m_created);
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "", e);
			msgreturn = new StringBuilder("@Error@"+e.getMessage());
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		} 
		return msgreturn.toString(); 
	}

	private boolean checkFrequencyType(int c_bpartner_Product_chip_ID){
		MBPartner_Product_Chip chip = new MBPartner_Product_Chip(getCtx(), c_bpartner_Product_chip_ID, get_TrxName());
		int frequencyRate=Character.getNumericValue(chip.getFrequencyType().charAt(0));
		char frequencyType=chip.getFrequencyType().charAt(1);
		p_DateStartServ=chip.getservice_start_date();
		LocalDate lastRun = null;
		if(chip.getLastRun()!=null)
			lastRun = chip.getLastRun().toLocalDateTime().toLocalDate();
		LocalDate ServiceStartDate = chip.getservice_start_date().toLocalDateTime().toLocalDate();
		LocalDate dateForCalc = ServiceStartDate;		
		LocalDate currentDateinusFrec = null;
		if(lastRun!=null)
			if(lastRun.isAfter(ServiceStartDate))
				dateForCalc=lastRun;		
		//Validacion de frecuencia Tipo M (Meses)
		if(frequencyType=='M') {
			log.warning("valido meses "+frequencyRate);
			FrequencyType=Calendar.MONTH;
			//1M (Mensualmente) frecuencia default siempre aplica
			if(frequencyRate==1)
				return true;
			//Otras frecuencias
			currentDateinusFrec=p_DateInvoiced.toLocalDateTime().toLocalDate().minusMonths(frequencyRate);
			log.warning(dateForCalc+"");
			log.warning(currentDateinusFrec+"");
			if (dateForCalc.isBefore(currentDateinusFrec) || dateForCalc.isEqual(currentDateinusFrec)) 
				return true;

		}
		//Validacion de frecuencia Tipo A (Años)
		if(frequencyType=='Y') {
			log.warning("valido años "+frequencyRate);
			FrequencyType=Calendar.YEAR;
			currentDateinusFrec=p_DateInvoiced.toLocalDateTime().toLocalDate().minusMonths(frequencyRate*(12));
			if (dateForCalc.isBefore(currentDateinusFrec) || dateForCalc.isEqual(currentDateinusFrec))
				return true;
			if (ServiceStartDate.getMonth()==p_DateInvoiced.toLocalDateTime().getMonth()
					&&
					ServiceStartDate.getYear()==p_DateInvoiced.toLocalDateTime().getYear())
				return true;
		}
		return false;
	}

	private void CreateInvoice(int start, int end){
		int idproduct, days, i =0;
		boolean save = false;				
		ArrayList<GroupProducts> pgrouped = new ArrayList<GroupProducts>();  		

		for(i=start; i < end; i++){
			StringBuffer descriptionline = new StringBuffer("");
			idproduct = invoicesdata.get(i).getIDProduct();
			days = 30;
			for(int n = 0; n < pgrouped.size(); n++){
				if(pgrouped.get(n).getDays() == days && pgrouped.get(n).getIDProduct() == idproduct){
					pgrouped.get(n).setQuantity(pgrouped.get(n).getIDQuantity() + invoicesdata.get(i).getQty());
					save = true;
					break;
				}
			}

			if(!save){	
				if(invoicesdata.get(i).getatributeDescription().compareTo("")!=0)
					descriptionline.append(invoicesdata.get(i).getatributeDescription()+". ");
				if(invoicesdata.get(i).getchipdescription().compareTo("")!=0)
					descriptionline.append(invoicesdata.get(i).getchipdescription()+". ");					
				pgrouped.add(new GroupProducts(days, invoicesdata.get(i).getQty(), idproduct, i, descriptionline));
			}
			save= false;
		}
		save = false;
		System.out.println("---");
		System.out.println(pgrouped.size());
		System.out.println("---");
		for( i= 0; i < pgrouped.size(); i++){

			int index = pgrouped.get(i).getIndex();
			save = createLine(pgrouped.get(i).getIDProduct(), pgrouped.get(i).getIDQuantity(), pgrouped.get(i).getDays(), 
					invoicesdata.get(index).getProductName() , invoicesdata.get(index).getIDPriceList(), 
					pgrouped.get(i).getDescriptionLine(), invoicesdata.get(index).getBreakValue());
		}
		System.out.println(order.getLines().length);

		if(save || order.getLines().length > 0)
			completeInvoice();
		else
			order.deleteEx(true);
	}				

	private boolean createLine (int idproduct, int quant,  int dayamount, String productname, 
			int price, StringBuffer descriptionline,BigDecimal breakValue)
	{
		System.out.println("--");
		System.out.println(idproduct);
		System.out.println(quant);
		System.out.println(dayamount);
		System.out.println(productname);
		System.out.println(price);
		System.out.println(descriptionline);
		System.out.println("--");
		if( quant > 0 && idproduct > 0 && dayamount > 0){
			MOrderLine line = new MOrderLine (order);			
			line.setProduct(new MProduct(getCtx(), idproduct, get_TrxName()));
			line.setQtyEntered(new BigDecimal(quant));
			line.setQty(new BigDecimal(quant));
			MProductPricing pp = new MProductPricing (line.getM_Product_ID(), order.getC_BPartner_ID(), line.getQtyEntered(), true,get_TrxName());
			pp.setM_PriceList_ID(order.getM_PriceList_ID());
			Timestamp orderDate = order.getDateOrdered();
			String sql = "SELECT plv.M_PriceList_Version_ID "
					+ "FROM M_PriceList_Version plv "
					+ "WHERE plv.M_PriceList_ID=? "						//	1
					+ " AND plv.ValidFrom <= ? "
					+ "ORDER BY plv.ValidFrom DESC";
			//	Use newest price list - may not be future

			int M_PriceList_Version_ID = DB.getSQLValueEx(null, sql, order.getM_PriceList_ID(), orderDate);
			if ( M_PriceList_Version_ID > 0 )
				pp.setM_PriceList_Version_ID(M_PriceList_Version_ID);
			pp.setPriceDate(orderDate);
			line.setPriceList(pp.getPriceList());
			line.setPriceLimit(pp.getPriceLimit());
			line.setPriceActual(pp.getPriceStd());
			line.setPriceEntered(pp.getPriceStd());
			if(breakValue.compareTo(Env.ZERO)<0) {
				line.setPriceActual(pp.getPriceStd().subtract(breakValue));
				line.setPriceEntered(pp.getPriceStd().subtract(breakValue));
			}
			line.setC_Currency_ID(pp.getC_Currency_ID());
			line.setDiscount(pp.getDiscount());
			line.setC_UOM_ID(pp.getC_UOM_ID());
			line.setTax();
			if (!line.save())
				log.severe("No se pudo obtener el precio para Producto: "+line.getM_Product().getName());
				//throw new IllegalStateException("No se pudo obtener el precio para Producto: "+line.getM_Product().getName()); 
			String linedescription="";
			if(Calendar.DAY_OF_MONTH>0){		
				String nombre = fcierre.getDisplayName(Calendar.MONTH,Calendar.LONG, new Locale("es", "ES"));				
				SimpleDateFormat df = new SimpleDateFormat("yy");
				linedescription=nombre.concat(" ").concat(df.format(fcierre.getTime())).toUpperCase();
			}
			//si no es frecuencia por defecto
			if(!(FrequencyType==Calendar.MONTH && FrequencyRate==1)){
				Calendar service_finish_date=Calendar.getInstance();
				service_finish_date.setTimeInMillis(p_DateStartServ.getTime());
				service_finish_date.add(FrequencyType, FrequencyRate);
				service_finish_date.add(Calendar.DAY_OF_MONTH,-1);
				String nombre = fcierre.getDisplayName(service_finish_date.MONTH,service_finish_date.LONG, new Locale("es", "ES"));				
				SimpleDateFormat df = new SimpleDateFormat("yy");
				linedescription=nombre.concat(" ").concat(df.format(fcierre.getTime())).toUpperCase();
			}
			line.setDescription(descriptionline.toString());	
			line.saveEx();
			//Description
			//line.addDescription(linedescription);
			this.addDescription(line, linedescription);  		//overwrite default function to replace invalid character
			if (!line.save()){				
				throw new IllegalStateException("Could not create Invoice Comment Line (sh)");
			}
			line.saveEx();
			line.updateHeaderTax();
			order.saveEx();
			if(breakValue.compareTo(Env.ZERO)>0)
				createDiscountLine(line, breakValue);

			if (log.isLoggable(Level.FINE)) log.fine(line.toString());
		}
		else{
			//addLog("no se pudo agregar una linea a la factura : " + invoice.getDocumentNo());			
			return false;
		}
		return true;
	}

	/**Complete Invoice */

	private void completeInvoice()
	{	    
		order.saveEx();
		order.load(this.get_TrxName());
		//order.load(order.get_TrxName(),null); // refresh from DB	
		if(p_docAction.compareTo(DocAction.ACTION_None)!=0)
			order.setDocAction(p_docAction);
			if (!order.processIt(p_docAction))
			{
				log.warning("completeOrder - failed: " + order);
				addLog(0, null, null,"completeOrder - failed: " + order, order.get_Table_ID(),order.getC_Order_ID());
				throw new IllegalStateException("Order Process Failed: " + order + " - " + order.getProcessMsg());	
			}
		log.warning("completeOrder : " + order+" action "+p_docAction);		
		order.saveEx();
		String message = Msg.parseTranslation(getCtx(), "@OrderProcessed@ " + order.getDocumentNo());
		addLog(order.getC_Order_ID(), order.getDateOrdered(), null, message, order.get_Table_ID(), order.getC_Order_ID());
		m_created++;

		order = null;
	}	//	completeInvoice

	/************************************************************************
	 ************* clase para a grupar los productos por dias **************
	 ***********************************************************************/
	public class GroupProducts {

		private int days, quantity, idproduct, index;
		private StringBuffer descriptionline;


		public GroupProducts (int days , int quantity, int idproduct, int index, StringBuffer descriptionline) {
			this.days = days ;
			this.quantity = quantity;
			this.idproduct = idproduct;
			this.index = index;
			this.descriptionline = descriptionline;
		}
		public int getDays(){
			return this.days;
		} 
		public int getIDQuantity(){
			return this.quantity;
		}
		public int getIDProduct(){
			return this.idproduct;
		}
		public int getIndex(){
			return this.index;
		}
		public StringBuffer getDescriptionLine(){
			return this.descriptionline;
		}
		public void setDays(int days){
			this.days = days;
		} 
		public void setQuantity(int quantity){
			this.quantity = quantity;
		} 
		public void setIDProduct(int idproduct){
			this.idproduct = idproduct;
		} 
		public void setIndex(int index){
			this.index = index;
		}
		public void setDescriptionLine(StringBuffer descriptionline){
			this.descriptionline = descriptionline;
		}
	}

	/************************************************************************
	 ************** clase que contiene los datos de la factura **************
	 ************************************************************************/
	public class InvoiceDataRecurrent{

		protected int idorg, iduser, idcurrency, idproduct, idcbpartner, idbillpartner, idpricelist, 
		idpartnerlocation, iddoctype, idproductchip,SalesRep_ID,C_PaymentTerm_ID,
		idbilluser,idbilllocation,qty;
		protected String productname, locationname, description, paymentrule, licenseplate,
		atributeDescription,contract_no,chipdescription;
		protected int a_asset_id;
		protected BigDecimal BreakValue;

		public int getA_asset_id() {
			return a_asset_id;
		}
		public void setA_asset_id(int a_asset_id) {
			this.a_asset_id = a_asset_id;
		}
		public InvoiceDataRecurrent( int idorg, int iduser,int idcurrency, int idcbpartner, int idbillpartner, int idproduct, 
				String productname, String paymentrule, int idpricelist, int idpartnerlocation, int iddoctype, String locationname, 
				String description, int idproductchip, String licenseplate,int SalesRep_ID,int C_PaymentTerm_ID,int a_asset_id,
				int idbilluser, int idbilllocation, int qty, String atributeDescription,String contract_no,String chipdescription,
				BigDecimal breakValue){
			this.idorg = idorg;
			this.iduser = iduser;
			this.idcbpartner = idcbpartner;
			this.idbillpartner = idbillpartner;
			this.idproduct = idproduct;
			this.productname = productname;
			this.idpricelist = idpricelist;
			this.paymentrule = paymentrule;
			this.idpartnerlocation = idpartnerlocation;
			this.locationname = locationname;
			this.description = description;
			this.idproductchip = idproductchip;
			this.licenseplate = licenseplate;
			this.SalesRep_ID=SalesRep_ID;
			this.C_PaymentTerm_ID=C_PaymentTerm_ID;
			this.a_asset_id=a_asset_id;
			this.idcurrency=idcurrency;
			this.iddoctype = iddoctype;
			this.idbilluser = idbilluser;
			this.idbilllocation = idbilllocation;
			this.qty = qty;
			this.atributeDescription = atributeDescription;
			this.contract_no = contract_no;
			this.chipdescription = chipdescription;
			this.BreakValue = breakValue;
		}
		public int getIDOrg(){
			return this.idorg;
		} 
		public int getIDUser(){
			return this.iduser;
		} 
		public int getIDCBPartner(){
			return this.idcbpartner;
		}
		public int getIDBillPartner(){
			return this.idbillpartner;
		}
		public int getIDProduct(){
			return this.idproduct;
		} 
		public String getProductName(){
			return this.productname;
		}
		public int getIDPriceList(){
			return this.idpricelist;
		} 
		public String getPaymentRule(){
			return this.paymentrule;
		}
		public int getIDPartnerLocation(){
			return this.idpartnerlocation;
		}
		public String getLocationName(){
			return this.locationname;
		}

		public String getDescription(){
			return this.description;
		}
		public int getIDCurrency(){
			return this.idcurrency;
		}
		public int getIDDoctype(){
			return this.iddoctype;
		}
		public int getIDProductChip(){
			return this.idproductchip;
		}

		public int getSalesRep_ID(){
			return this.SalesRep_ID;

		}
		public int getC_PaymentTerm_ID(){
			return this.C_PaymentTerm_ID;

		}
		public int getC_DocType_ID(){
			return this.iddoctype;

		}	

		public int getBill_Location_ID(){
			return this.idbilllocation;			
		}

		public int getBill_User_ID(){
			return this.idbilluser;			
		}	

		public int getQty(){
			return this.qty;			
		}			

		public String getLicensePlate(){
			return this.licenseplate;
		}

		public String getatributeDescription(){
			return this.atributeDescription;
		}

		public BigDecimal getBreakValue(){
			return this.BreakValue;
		}
		/**                          **/
		public void setIDOrg(int org){
			this.idorg = org;
		}
		public void setIDUser(int user){
			this.iduser = user;
		}
		public void setIDCBPartner(int p){
			this.idcbpartner = p;
		}
		public void setIDBillPartner(int p){
			this.idbillpartner = p;
		}
		public void setIDProduct(int p){
			this.idproduct = p;
		}
		public void setProductName(String name){
			this.productname = name;
		}
		public void setIDPaymentRule(String paymentrule){
			this.paymentrule = paymentrule;
		}
		public void setIDPriceList(int p){
			this.idpricelist = p;
		}
		public void setIDPartnerLocation(int idlocation){
			this.idpartnerlocation = idlocation;
		}
		public void setLocationName(String name){
			this.locationname = name;
		}
		public void setLDescription(String description){
			this.description = description;
		}
		public void setIDCurrenct(int currency){
			this.idcurrency = currency;
		}
		public void setIDDoctype(int d){
			this.iddoctype = d;
		}
		public void setIDProductChip(int p){
			this.idproductchip = p;
		}
		public void setLicensePlate(String l){
			this.licenseplate = l;
		}
		public void setSalesRep_ID(int sr){
			this.SalesRep_ID=sr;
		}
		public void setC_PaymentTerm_ID(int cpt){
			this.C_PaymentTerm_ID=cpt;
		}
		public void setC_Doctype_ID(int iddoctype){
			this.iddoctype=iddoctype;
		}		

		public void setBill_Location_ID(int idbilllocation){
			this.idbilllocation=idbilllocation;			
		} 

		public void setBill_User_ID(int idbilluser){
			this.idbilluser=idbilluser;			
		}	

		public void setQty(int qty){
			this.qty = qty;
		}

		public void setatributeDescription(String atributeDescription){
			this.atributeDescription = atributeDescription;
		}

		public void setcontract_no(String contract_no){
			this.contract_no = contract_no;
		}

		public String getcontract_no(){
			return this.contract_no;
		}

		public String getchipdescription() {
			return this.chipdescription;
		}

		public void setchipdescription(String chipdescription) {
			this.chipdescription = chipdescription;
		}

		public void setchipdescription(BigDecimal breakValue) {
			this.BreakValue = breakValue;
		}

	}

	protected void updateChipLastRun() {
		if(invoicesdata.size() >= 0)
			for(int i=0;i<invoicesdata.size();i++) {
				DB.executeUpdate("UPDATE C_BPartner_Product_Chip SET updated=now(),updatedby="+getAD_User_ID()+",lastrun = '"+p_DateInvoiced+
						"' WHERE C_BPartner_Product_Chip_ID = ?",
						invoicesdata.get(i).getIDProductChip(),
						get_TrxName());
			}
	}

	private boolean createDiscountLine (MOrderLine ParentLine,BigDecimal breakValue)
	{		
		if(breakValue==null)
			breakValue=BigDecimal.ZERO;
		if(ProductDiscountID==0)
			throw new AdempiereException("No RECURRENT_PRODUCT_DISCOUNT_ID is set in system configuration");
		MOrderLine line = new MOrderLine (order);			
		line.setProduct(new MProduct(getCtx(), ProductDiscountID,null));
		line.setQty(new BigDecimal(1).negate());
		//line.setPriceList(breakValue.negate());
		line.setPriceLimit(Env.ZERO);	
		line.setPriceActual(breakValue);
		line.saveEx();
		line.setPriceEntered(breakValue);		
		line.saveEx();
		line.setC_Currency_ID(ParentLine.getC_Currency_ID());
		line.setDiscount(Env.ZERO);		
		line.setTax();

		
		String linedescription="Descuento para Producto: "+ParentLine.getProduct().getName();
		line.saveEx();
		//if (!line.save())
			//throw new IllegalStateException("Error Creando descuento"+linedescription+" a tercero: "+order.getC_BPartner().getValue()+" - "+order.getC_BPartner().getName());
		//Description
		line.addDescription(linedescription);				
		if (!line.save()){				
			throw new IllegalStateException("Could not create Invoice Comment Line (sh)");
		}
		line.saveEx();
		line.updateHeaderTax();
		order.saveEx();
		if (log.isLoggable(Level.FINE)) log.fine(line.toString());
		return true;
	}

	private void addDescription (MOrderLine line,String description)
	{
		String desc = line.getDescription();
		if (desc == null)
			line.setDescription(description);
		else
			line.setDescription(desc + " - " + description);
	}	//	addDescription
}
