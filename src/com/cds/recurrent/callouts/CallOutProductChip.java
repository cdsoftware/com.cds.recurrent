package com.cds.recurrent.callouts;

import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartner;

public class CallOutProductChip implements IColumnCallout{

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
		if(mField.getColumnName().equals("C_BPartner_ID")){
			return GetInfo(ctx,mTab);
		}
		return null;
	}
	private String GetInfo(Properties ctx,GridTab mTab){
		
		if(mTab.getValue("C_BPartner_ID")!=null){
			Object BPartner = mTab.getValue("C_BPartner_ID");
			MBPartner partner = new MBPartner(ctx, (Integer)BPartner, null);			
			mTab.setValue("C_BPartner_Location_ID", partner.getPrimaryC_BPartner_Location().get_ID());			
			mTab.setValue("AD_User_ID", partner.getPrimaryAD_User_ID());
			mTab.setValue("Bill_BPartner_ID", partner.get_ID());			
			mTab.setValue("Bill_User_ID", partner.getPrimaryAD_User_ID());
			mTab.setValue("Bill_Location_ID", partner.getPrimaryC_BPartner_Location().get_ID());
			
		}
		return null;
	}
}
