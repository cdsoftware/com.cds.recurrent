package com.cds.recurrent.callouts;

import org.adempiere.base.IColumnCallout;
import org.adempiere.base.IColumnCalloutFactory;

public class CalloutFactory implements IColumnCalloutFactory{

	@Override
	public IColumnCallout[] getColumnCallouts(String tableName, String columnName) {
		if(tableName.equals("C_BPartner_Product_Chip"))
			if (columnName.equalsIgnoreCase("C_BPartner_ID"))
				return new IColumnCallout[]{new CallOutProductChip()};
		return new IColumnCallout[0];
	}

}
